import 'package:alarm/alarm.dart';
import 'package:chilleddrink/Dataclasses/sentry_options.dart';
import 'package:chilleddrink/Dataclasses/shared_prefs.dart';
import 'package:chilleddrink/Widgets/home_page.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await SharedPrefs.init();
  await Alarm.init();
  await SentryFlutter.init((options) {
    options.dsn = sentryDNS;
    options.tracesSampleRate = 1.0;
  }, appRunner: () => runApp(const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return FeatureDiscovery(
      child: MaterialApp(
        title: 'Chilled Drink Timer',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.lightBlue),
          useMaterial3: true,
        ),
        home: const HomePage(),
      ),
    );
  }
}
