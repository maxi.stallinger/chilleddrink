import 'package:chilleddrink/Widgets/general/general_gradient_range_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomDescribedRangeSlider extends StatefulWidget {
  final Text title;
  final String? valueSuffix;
  final RangeValues values;
  final double min;
  final double max;
  final int? divisions;
  final int roundToPrecision;
  final CrossAxisAlignment crossAxisAlignment;
  final Function(RangeValues values) onChanged;
  const CustomDescribedRangeSlider(
      {super.key,
      required this.title,
      this.valueSuffix,
      required this.values,
      required this.min,
      required this.max,
      this.divisions,
      this.roundToPrecision = 0,
      this.crossAxisAlignment = CrossAxisAlignment.center,
      required this.onChanged});

  @override
  State<CustomDescribedRangeSlider> createState() => _CustomDescribedRangeSliderState();
}

class _CustomDescribedRangeSliderState extends State<CustomDescribedRangeSlider> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: widget.crossAxisAlignment,
      children: [
        widget.title,
        const SizedBox(height: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Text(
                    "${widget.values.start.round().toString()} ${widget.valueSuffix}",
                    textAlign: TextAlign.end,
                  ),
                  Text(
                    "End",
                    textAlign: TextAlign.end,
                    style: GoogleFonts.raleway(fontWeight: FontWeight.w500, fontSize: 11),
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 6,
              child: SliderTheme(
                data: SliderThemeData(
                  rangeTrackShape: const GradientRectangularRangeSliderTrackShape(
                    gradient: LinearGradient(
                      colors: [Color.fromARGB(255, 116, 211, 255), Colors.red],
                      begin: Alignment(-0.7, 0),
                      end: Alignment(0.4, 0),
                    ),
                  ),
                  inactiveTrackColor: const Color.fromARGB(255, 222, 228, 235),
                  valueIndicatorColor: Colors.lightBlueAccent,
                  rangeValueIndicatorShape: const PaddleRangeSliderValueIndicatorShape(),
                  valueIndicatorTextStyle: GoogleFonts.raleway(fontWeight: FontWeight.w500, fontSize: 11),
                ),
                child: RangeSlider(
                  min: widget.min,
                  max: widget.max,
                  divisions: widget.divisions,
                  labels: const RangeLabels(
                    "End",
                    "Start",
                  ),
                  activeColor: Colors.lightBlueAccent,
                  values: widget.values,
                  onChanged: (RangeValues values) {
                    widget.onChanged(values);
                  },
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Text(
                    "${widget.values.end.round().toString()} ${widget.valueSuffix}",
                    textAlign: TextAlign.end,
                  ),
                  Text(
                    "Start",
                    textAlign: TextAlign.end,
                    style: GoogleFonts.raleway(fontWeight: FontWeight.w500, fontSize: 11),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
