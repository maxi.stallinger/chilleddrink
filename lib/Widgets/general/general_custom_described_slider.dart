import 'package:flutter/material.dart';

class CustomDescribedSlider extends StatefulWidget {
  final Text title;
  final String? valueSuffix;
  final double value;
  final double min;
  final double max;
  final int? divisions;
  final double flexRatio;
  final int roundToPrecision;
  final CrossAxisAlignment crossAxisAlignment;
  final Function(double value) onChanged;
  const CustomDescribedSlider(
      {super.key,
      required this.title,
      this.valueSuffix,
      required this.value,
      required this.min,
      required this.max,
      this.divisions,
      this.flexRatio = 1 / 4,
      this.roundToPrecision = 0,
      this.crossAxisAlignment = CrossAxisAlignment.center,
      required this.onChanged});

  @override
  State<CustomDescribedSlider> createState() => _CustomDescribedSliderState();
}

class _CustomDescribedSliderState extends State<CustomDescribedSlider> {
  String roundedNumber(double num) {
    return (widget.roundToPrecision == 0)
        ? num.round().toString()
        : double.parse(num.toStringAsFixed(widget.roundToPrecision)).toString();
  }

  @override
  Widget build(BuildContext context) {
    String labeltext = roundedNumber(widget.value);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: widget.crossAxisAlignment,
      children: [
        widget.title,
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 1,
              child: Text(
                "$labeltext ${widget.valueSuffix}",
                textAlign: TextAlign.center,
              ),
            ),
            Flexible(
              flex: (1 / widget.flexRatio).round(),
              child: Slider(
                min: widget.min,
                max: widget.max,
                divisions: widget.divisions,
                label: labeltext,
                activeColor: Colors.lightBlueAccent,
                thumbColor: Colors.lightBlueAccent,
                value: widget.value,
                onChanged: (double value) {
                  widget.onChanged(value);
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
