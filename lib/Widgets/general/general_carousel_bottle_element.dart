import 'package:chilleddrink/Dataclasses/bottle.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CarouselBottleElement extends StatelessWidget {
  final Bottle bottle;
  final bool isActive;
  const CarouselBottleElement({super.key, required this.bottle, required this.isActive});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            flex: 3,
            child: Icon(
              bottle.icon,
              color: isActive ? Colors.lightBlueAccent : null,
              size: 70,
            ),
          ),
          Flexible(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Text(
                bottle.displayName,
                textAlign: TextAlign.center,
                style: GoogleFonts.roboto(fontSize: 12),
                maxLines: 2,
                overflow: TextOverflow.fade,
                softWrap: true,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
