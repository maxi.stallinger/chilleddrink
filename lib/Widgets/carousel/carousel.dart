import 'package:chilleddrink/Dataclasses/cooling_data.dart';
import 'package:chilleddrink/Widgets/carousel/carousel_graph.dart';
import 'package:chilleddrink/Widgets/carousel/carousel_timer.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class Carousel extends StatefulWidget {
  final CoolingData coolingData;
  const Carousel({super.key, required this.coolingData});

  @override
  State<Carousel> createState() => _CarouselState();
}

class _CarouselState extends State<Carousel> {
  final PageController _carouselController = PageController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Indicator
        Flexible(
          flex: 3,
          child: SmoothPageIndicator(
            controller: _carouselController,
            count: 2,
            effect: ExpandingDotsEffect(
              activeDotColor: Colors.grey.shade700,
              dotColor: Colors.grey.shade400,
              dotWidth: 6,
              dotHeight: 6,
            ),
          ),
        ),

        // Carousel
        Flexible(
          flex: 10,
          child: PageView(
            pageSnapping: true,
            controller: _carouselController,
            children: [
              CarouselTimer(coolingData: widget.coolingData),
              CarouselGraph(coolingData: widget.coolingData),
            ],
          ),
        ),
        const Flexible(
          flex: 1,
          child: SizedBox(),
        )
      ],
    );
  }
}
