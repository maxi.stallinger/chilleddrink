import 'package:chilleddrink/Dataclasses/chart_data.dart';
import 'package:chilleddrink/Dataclasses/cooling_data.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class CarouselGraph extends StatefulWidget {
  final CoolingData coolingData;
  const CarouselGraph({super.key, required this.coolingData});

  @override
  State<CarouselGraph> createState() => _CarouselGraphState();
}

class _CarouselGraphState extends State<CarouselGraph> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      padding: const EdgeInsets.symmetric(vertical: 25),
      child: FittedBox(
        child: SizedBox(
          height: 300,
          width: 350,
          child: SfCartesianChart(
            legend: const Legend(
              isVisible: true,
              position: LegendPosition.bottom,
              toggleSeriesVisibility: false,
            ),
            plotAreaBorderWidth: 0,
            tooltipBehavior: TooltipBehavior(
              enable: true,
              header: '',
              format: "point.y\npoint.x",
            ),
            primaryXAxis: DateTimeAxis(
              majorGridLines: const MajorGridLines(width: 0),
              dateFormat: DateFormat.Hm(),
            ),
            primaryYAxis: NumericAxis(
              minimum: 6,
              maximum: widget.coolingData.startTemperature * 1.2,
              axisLine: const AxisLine(color: Colors.transparent, dashArray: <double>[5, 5]),
              majorGridLines: MajorGridLines(width: 0.7, color: Colors.grey.shade400, dashArray: const <double>[5, 5]),
              edgeLabelPlacement: EdgeLabelPlacement.shift,
              labelFormat: '{value}°C',
              majorTickLines: const MajorTickLines(size: 0),
            ),
            palette: const [
              Colors.lightBlueAccent,
            ],
            series: <ChartSeries>[
              // Renders spline chart
              SplineSeries<ChartData, DateTime>(
                name: "Temperature",
                dataSource: widget.coolingData.chartData,
                xValueMapper: (ChartData data, _) => data.time,
                yValueMapper: (ChartData data, _) => data.temperature,
                markerSettings: const MarkerSettings(
                  isVisible: true,
                  borderWidth: 3,
                  height: 10,
                  width: 10,
                ),
                width: 3,
                animationDuration: 800,
                animationDelay: 250,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
