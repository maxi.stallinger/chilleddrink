import 'package:chilleddrink/Dataclasses/cooling_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class CarouselTimer extends StatefulWidget {
  final CoolingData coolingData;
  const CarouselTimer({super.key, required this.coolingData});

  @override
  State<CarouselTimer> createState() => _CarouselTimerState();
}

class _CarouselTimerState extends State<CarouselTimer> {
  @override
  void initState() {
    widget.coolingData.countdown.addListener(countdownChanged);
    super.initState();
  }

  void countdownChanged() {
    if (mounted) {
      setState(() {
        widget.coolingData.countdown.remainingProgress = widget.coolingData.countdown.value;
        widget.coolingData.updateCurrentTemperature();
      });
    }
  }

  @override
  void dispose() {
    widget.coolingData.countdown.removeListener(countdownChanged);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      padding: const EdgeInsets.symmetric(vertical: 25),
      child: FittedBox(
        child: SizedBox.square(
          dimension: 300,
          child: Stack(
            alignment: AlignmentDirectional.center,
            children: [
              // Progress Indicator
              Positioned.fill(
                child: CircularProgressIndicator(
                  value: widget.coolingData.countdown.remainingProgress,
                  backgroundColor: Colors.grey.shade200,
                  color: Colors.lightBlueAccent,
                  strokeWidth: 6,
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // Current Temperature
                  Padding(
                    padding: const EdgeInsets.only(top: 42),
                    child: Text(
                      "${num.parse(widget.coolingData.currentTemperature.toStringAsFixed(1))} °C",
                      style: GoogleFonts.roboto(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  // Time Display
                  AnimatedBuilder(
                    animation: widget.coolingData.countdown,
                    builder: (context, child) => Text(
                      widget.coolingData.countdown.text,
                      style: GoogleFonts.robotoCondensed(
                        color: Colors.black,
                        fontSize: 50,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                  ),
                  // Finishing Time
                  Padding(
                    padding: const EdgeInsets.only(top: 40),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Icon(
                          Icons.alarm_rounded,
                          color: Colors.lightBlueAccent,
                          size: 16,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        Text(
                          DateFormat("HH : mm").format(widget.coolingData.countdown.estimatedTimeOfArrival).toString(),
                          style: GoogleFonts.roboto(
                            color: Colors.black,
                            fontSize: 18,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
