import 'package:chilleddrink/Dataclasses/custom_icons_icons.dart';
import 'package:chilleddrink/Dataclasses/cooling_data.dart';
import 'package:chilleddrink/Widgets/configurator/configurator_bottom_sheet.dart';
import 'package:flutter/material.dart';
import 'package:chilleddrink/Dataclasses/bottle.dart';

class CoolingConfigurator extends StatelessWidget {
  final CoolingData coolingData;
  const CoolingConfigurator({super.key, required this.coolingData});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Align(
        alignment: Alignment.bottomCenter,
        child: FittedBox(
          child: SizedBox(
            width: 360,
            height: 85,
            child: Card(
              color: Colors.white,
              surfaceTintColor: Colors.white,
              elevation: 4.0,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
              clipBehavior: Clip.antiAlias,
              child: InkWell(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 27,
                      child: ListTile(
                        visualDensity: const VisualDensity(horizontal: -4),
                        leading: coolingData.activeBottle.material == BottleMaterial.glass
                            ? const Icon(CustomIcons.beer_bottle, size: 50, color: Colors.lightBlue)
                            : const Icon(CustomIcons.beer_can, size: 50, color: Colors.lightBlue),
                        title: Text(
                          coolingData.activeBottle.displayName,
                          style: const TextStyle(fontSize: 10),
                          maxLines: 1,
                          overflow: TextOverflow.fade,
                          softWrap: false,
                        ),
                        subtitle: Text(
                          "${coolingData.startTemperature.toInt()} °C ➞ ${coolingData.endTemperature.toInt()} °C",
                          style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 22,
                      child: ListTile(
                        visualDensity: const VisualDensity(horizontal: -4),
                        leading: const Icon(CustomIcons.freezer, size: 50, color: Colors.lightBlue),
                        title: const Text(
                          "Freezer",
                          style: TextStyle(fontSize: 10),
                          textAlign: TextAlign.start,
                        ),
                        subtitle: Text(
                          "${coolingData.freezerTemperature.toInt()} °C",
                          style: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                          textAlign: TextAlign.start,
                        ),
                      ),
                    ),
                  ],
                ),
                onTap: () {
                  if (coolingData.countdown.isAnimating == false) {
                    showModalBottomSheet(
                      isScrollControlled: true,
                      shape:
                          const RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(20))),
                      context: context,
                      builder: (context) => CoolingConfiguratorBottomSheet(coolingData: coolingData),
                    ).then((value) => coolingData.notifyListeners());
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: const Text(
                          "Reset Timer to make changes",
                          style: TextStyle(color: Colors.lightBlueAccent, fontSize: 16),
                          textAlign: TextAlign.center,
                        ),
                        elevation: 20,
                        behavior: SnackBarBehavior.floating,
                        margin: const EdgeInsets.all(30),
                        duration: const Duration(milliseconds: 1600),
                        backgroundColor: Colors.grey[800],
                        showCloseIcon: true,
                        shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15))),
                      ),
                    );
                  }
                },
              ),
            ),
          ),
        ),
      ),
    );
  }
}
