import 'package:carousel_slider/carousel_slider.dart';
import 'package:chilleddrink/Dataclasses/cooling_data.dart';
import 'package:chilleddrink/Widgets/general/general_carousel_bottle_element.dart';
import 'package:chilleddrink/Widgets/general/general_custom_described_rangeslider.dart';
import 'package:chilleddrink/Widgets/general/general_custom_described_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class CoolingConfiguratorBottomSheet extends StatefulWidget {
  final CoolingData coolingData;
  const CoolingConfiguratorBottomSheet({super.key, required this.coolingData});

  @override
  State<CoolingConfiguratorBottomSheet> createState() => _CoolingConfiguratorBottomSheetState();
}

class _CoolingConfiguratorBottomSheetState extends State<CoolingConfiguratorBottomSheet> {
  final CarouselController _controller = CarouselController();
  late int _currentPageIndex = widget.coolingData.bottlesList.indexOf(widget.coolingData.activeBottle);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Container(
          width: double.infinity,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
          ),
          child: Padding(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20, 25, 20, 25),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Caption
                  Text(
                    "Cooling Configurator",
                    style: GoogleFonts.roboto(fontWeight: FontWeight.w500, fontSize: 22),
                  ),
                  // Bottle-Selection
                  Column(
                    children: [
                      CarouselSlider.builder(
                        itemCount: widget.coolingData.bottlesList.length - 1,
                        carouselController: _controller,
                        options: CarouselOptions(
                          aspectRatio: 2 / 1,
                          viewportFraction: 1 / 2.7,
                          enableInfiniteScroll: false,
                          enlargeCenterPage: true,
                          enlargeFactor: 0.45,
                          initialPage: _currentPageIndex,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _currentPageIndex = index;
                            });
                          },
                        ),
                        itemBuilder: (context, index, realIndex) {
                          bool isActive =
                              (widget.coolingData.bottlesList.elementAt(index) == widget.coolingData.activeBottle);
                          return InkResponse(
                            radius: 25,
                            child: CarouselBottleElement(
                              bottle: widget.coolingData.bottlesList.elementAt(index),
                              isActive: isActive,
                            ),
                            onTap: () {
                              _controller.animateToPage(
                                realIndex,
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.linearToEaseOut,
                              );
                              setState(() {
                                widget.coolingData.activeBottle = widget.coolingData.bottlesList.elementAt(realIndex);
                                widget.coolingData.endTemperature = widget.coolingData.activeBottle.desiredTemperature;
                                widget.coolingData.startTemperature = 25;
                                widget.coolingData.freezerTemperature = -18;
                              });
                            },
                          );
                        },
                      ),
                      Center(
                        child: AnimatedSmoothIndicator(
                          activeIndex: _currentPageIndex,
                          count: widget.coolingData.bottlesList.length - 1,
                          effect: ExpandingDotsEffect(
                            activeDotColor: Colors.lightBlueAccent,
                            dotColor: Colors.grey.shade400,
                            dotWidth: 5,
                            dotHeight: 5,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 45,
                  ),
                  // Drink Temperature
                  CustomDescribedRangeSlider(
                    title: Text(
                      "Drink Temperature",
                      style: GoogleFonts.roboto(fontWeight: FontWeight.w300, fontSize: 17),
                    ),
                    valueSuffix: "°C",
                    values: RangeValues(widget.coolingData.endTemperature, widget.coolingData.startTemperature),
                    min: 6,
                    max: 35,
                    divisions: 35 - 6,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    onChanged: (values) {
                      setState(() {
                        widget.coolingData.endTemperature = values.start.round().toDouble();
                        widget.coolingData.startTemperature = values.end.round().toDouble();
                        widget.coolingData.freezerTemperature = -18;
                      });
                    },
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  // Freezer Temperature
                  CustomDescribedSlider(
                    title: Text(
                      "Freezer Temperature",
                      style: GoogleFonts.roboto(fontWeight: FontWeight.w300, fontSize: 17),
                    ),
                    value: widget.coolingData.freezerTemperature,
                    min: -25,
                    max: widget.coolingData.endTemperature - 1,
                    divisions: (widget.coolingData.endTemperature.toInt() - 1) + 25,
                    valueSuffix: "°C",
                    crossAxisAlignment: CrossAxisAlignment.start,
                    onChanged: (value) {
                      setState(() {
                        widget.coolingData.freezerTemperature = value;
                      });
                    },
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  // Accept Button
                  Center(
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.lightBlueAccent,
                          foregroundColor: Colors.black,
                          fixedSize: const Size(280, 40),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5))),
                      child: Text(
                        "Apply",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w500, fontSize: 18),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
