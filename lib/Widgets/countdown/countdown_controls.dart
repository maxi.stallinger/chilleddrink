import 'package:alarm/alarm.dart';
import 'package:chilleddrink/Dataclasses/cooling_data.dart';
import 'package:chilleddrink/Dataclasses/shared_prefs.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:disable_battery_optimization/disable_battery_optimization.dart';

class CountdownControls extends StatefulWidget {
  final CoolingData coolingData;
  const CountdownControls({super.key, required this.coolingData});

  @override
  State<CountdownControls> createState() => _CountdownControlsState();
}

class _CountdownControlsState extends State<CountdownControls> with TickerProviderStateMixin, WidgetsBindingObserver {
  bool _isActivated = false;
  late AnimationController _animationController;
  AlarmSettings alarmSettings(DateTime time) => AlarmSettings(
        id: 1,
        dateTime: time,
        loopAudio: true,
        vibrate: true,
        volumeMax: false,
        fadeDuration: 2.0,
        assetAudioPath: "assets/alarm.mp3",
        notificationTitle: "Ready to drink 🍺",
        notificationBody: "Take it out of the freezer ❄",
        enableNotificationOnKill: true,
      );

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed && _isActivated) {
      SharedPrefs.readScheduledTime(widget.coolingData);
      widget.coolingData.countdown.reverse(from: widget.coolingData.countdown.remainingProgress);
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _animationController = AnimationController(vsync: this, duration: const Duration(milliseconds: 500));
  }

  @override
  void dispose() {
    _animationController.dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void startCountdown() async {
    _isActivated = true;
    _animationController.forward();
    widget.coolingData.countdown.reverse(from: widget.coolingData.countdown.value);
    setState(() {
      widget.coolingData.updateChartData();
    });
    await Alarm.set(alarmSettings: alarmSettings(widget.coolingData.chartData.last.time));
    await SharedPrefs.writeScheduledTime(widget.coolingData);
    await SharedPrefs.writeLastUsedBottle(widget.coolingData);
  }

  void resetCountdown() async {
    _animationController.reverse();
    widget.coolingData.countdown.reset();
    setState(() {
      _isActivated = false;
      widget.coolingData.countdown.value = 1.0;
      widget.coolingData.countdown.remainingProgress = 1.0;
      widget.coolingData.updateChartData();
      widget.coolingData.updateCurrentTemperature();
    });
    await Alarm.stop(1);
    await SharedPrefs.removeScheduledTime();
  }

  _showDisableBatteryOptimizationsMenu(BuildContext context) {
    final alert = AlertDialog(
      title: const Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Icon(Icons.warning_amber_rounded, color: Colors.red),
          SizedBox(width: 10),
          Text("Attention"),
        ],
      ),
      content: RichText(
        text: const TextSpan(
          text: 'Please disable battery optimization\n\n',
          style: TextStyle(color: Colors.black),
          children: <TextSpan>[
            TextSpan(text: 'Otherwise, the alarm is '),
            TextSpan(text: 'not', style: TextStyle(fontWeight: FontWeight.bold, decoration: TextDecoration.underline)),
            TextSpan(text: ' going to work!'),
          ],
        ),
      ),
      actions: [
        TextButton(
          child: const Text("Disable it", style: TextStyle(color: Colors.red)),
          onPressed: () {
            Navigator.of(context).pop();
            DisableBatteryOptimization.showDisableBatteryOptimizationSettings();
          },
        ),
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topCenter,
      child: SizedBox(
        height: 55,
        width: 130,
        child: ElevatedButton.icon(
          icon: AnimatedIcon(
            icon: AnimatedIcons.play_pause,
            progress: _animationController,
            color: Colors.black,
            size: 35,
          ),
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(_isActivated ? Colors.lightBlueAccent.shade100 : null),
            elevation: MaterialStateProperty.all(8.0),
            surfaceTintColor: MaterialStateProperty.all(
              Colors.transparent,
            ),
          ),
          label: Text(
            _isActivated ? "Reset" : "Start ",
            style: GoogleFonts.roboto(
              color: Colors.black,
              fontWeight: FontWeight.normal,
              fontSize: 17,
            ),
          ),
          onPressed: () {
            if (_animationController.status == AnimationStatus.dismissed) {
              startCountdown();
              DisableBatteryOptimization.isBatteryOptimizationDisabled.then((bool? disabled) {
                if (disabled == false) _showDisableBatteryOptimizationsMenu(context);
              });
            } else {
              resetCountdown();
            }
          },
        ),
      ),
    );
  }
}
