import 'package:chilleddrink/Dataclasses/cooling_data.dart';
import 'package:chilleddrink/Widgets/settings/settings_page.dart';
import 'package:flutter/material.dart';

class SettingsButton extends StatelessWidget {
  final CoolingData coolingData;
  const SettingsButton({super.key, required this.coolingData});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.settings, color: Colors.grey.shade500, size: 32),
      onPressed: () {
        if (coolingData.countdown.isAnimating == false) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => SettingsPage(coolingData: coolingData),
            ),
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: const Text(
                "Reset Timer to make changes",
                style: TextStyle(color: Colors.lightBlueAccent, fontSize: 16),
                textAlign: TextAlign.center,
              ),
              elevation: 20,
              behavior: SnackBarBehavior.floating,
              margin: const EdgeInsets.all(30),
              duration: const Duration(milliseconds: 1600),
              backgroundColor: Colors.grey[800],
              showCloseIcon: true,
              shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15))),
            ),
          );
        }
      },
    );
  }
}
