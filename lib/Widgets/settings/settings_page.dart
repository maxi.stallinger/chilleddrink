import 'package:carousel_slider/carousel_slider.dart';
import 'package:chilleddrink/Dataclasses/bottle.dart';
import 'package:chilleddrink/Dataclasses/cooling_data.dart';
import 'package:chilleddrink/Dataclasses/shared_prefs.dart';
import 'package:chilleddrink/Widgets/general/general_carousel_bottle_element.dart';
import 'package:chilleddrink/Widgets/general/general_custom_described_slider.dart';
import 'package:chilleddrink/Widgets/settings/settings_page_boxelement.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class SettingsPage extends StatefulWidget {
  final CoolingData coolingData;
  const SettingsPage({super.key, required this.coolingData});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  double _freezerTemperature = -18;
  double _startingTemperature = 25;
  late double _endingTemperature;
  late int _elapsedMinutes;
  final TextEditingController _displayNameController = TextEditingController();

  final CarouselController _controller = CarouselController();
  late int _currentPageIndex;
  late Bottle _bottleToChange;
  late Bottle _tempBottle;

  @override
  void initState() {
    // Init to last bottle (add)
    _currentPageIndex = widget.coolingData.bottlesList.length - 1;
    _bottleToChange = widget.coolingData.bottlesList.last;
    _tempBottle = _bottleToChange.copyWith(isDeleteProtected: false);
    _displayNameController.text = _tempBottle.displayName;
    _endingTemperature = _tempBottle.desiredTemperature;
    _elapsedMinutes = _tempBottle.approximateDuration(_freezerTemperature, _startingTemperature).inMinutes.round();
    super.initState();
  }

  void acceptOverwrite() {
    int insertPosition = 0;
    if (_bottleToChange == widget.coolingData.bottlesList.last) {
      setState(() {
        insertPosition = widget.coolingData.bottlesList.length - 1;
        widget.coolingData.bottlesList.insert(insertPosition, _tempBottle);
        widget.coolingData.bottlesList
            .elementAt(insertPosition)
            .updateCoefficient(_startingTemperature, _endingTemperature, _freezerTemperature, _elapsedMinutes * 60);
        widget.coolingData.bottlesList.last.displayName = "Custom Bottle ${widget.coolingData.bottlesList.length - 2}";
        _bottleToChange = widget.coolingData.bottlesList.elementAt(insertPosition);
        _tempBottle = _bottleToChange.copyWith(isDeleteProtected: false);
        _displayNameController.text = _tempBottle.displayName;
        _endingTemperature = _bottleToChange.desiredTemperature;
        _elapsedMinutes = _tempBottle.approximateDuration(_freezerTemperature, _startingTemperature).inMinutes.round();
        _currentPageIndex = insertPosition - 2;
      });
    } else {
      setState(() {
        insertPosition = widget.coolingData.bottlesList.indexOf(_bottleToChange);
        widget.coolingData.bottlesList[insertPosition] = _tempBottle;
        widget.coolingData.bottlesList
            .elementAt(insertPosition)
            .updateCoefficient(_startingTemperature, _endingTemperature, _freezerTemperature, _elapsedMinutes * 60);
        widget.coolingData.bottlesList.last.displayName = "Custom Bottle ${widget.coolingData.bottlesList.length - 2}";
        _bottleToChange = widget.coolingData.bottlesList.elementAt(insertPosition);
        _tempBottle = _bottleToChange.copyWith();
        _displayNameController.text = _tempBottle.displayName;
        _endingTemperature = _bottleToChange.desiredTemperature;
        _elapsedMinutes = _tempBottle.approximateDuration(_freezerTemperature, _startingTemperature).inMinutes.round();
        _currentPageIndex = insertPosition - 2;
      });
    }
    widget.coolingData.activeBottle = widget.coolingData.bottlesList.elementAt(insertPosition);
    widget.coolingData.startTemperature = 25;
    widget.coolingData.freezerTemperature = -18;
    widget.coolingData.notifyListeners();
    SharedPrefs.writeBottlesList(widget.coolingData);
  }

  void acceptDelete() {
    int indexWhereDelete = 0;
    setState(() {
      indexWhereDelete = widget.coolingData.bottlesList.indexOf(_bottleToChange);
      widget.coolingData.bottlesList.removeAt(indexWhereDelete);
      widget.coolingData.bottlesList.last.displayName = "Custom Bottle ${widget.coolingData.bottlesList.length - 2}";
      _bottleToChange = widget.coolingData.bottlesList.elementAt(indexWhereDelete);
      _tempBottle = _bottleToChange.copyWith(
          isDeleteProtected: (indexWhereDelete == widget.coolingData.bottlesList.length - 1) ? false : null);
      _displayNameController.text = _tempBottle.displayName;
      _endingTemperature = _bottleToChange.desiredTemperature;
      _elapsedMinutes = _tempBottle.approximateDuration(_freezerTemperature, _startingTemperature).inMinutes.round();
    });
    widget.coolingData.activeBottle = widget.coolingData.bottlesList.elementAt(indexWhereDelete - 1);
    widget.coolingData.startTemperature = 25;
    widget.coolingData.freezerTemperature = -18;
    widget.coolingData.notifyListeners();
    SharedPrefs.writeBottlesList(widget.coolingData);
  }

  @override
  Widget build(BuildContext context) {
    // Alert Buttons
    Widget cancelButton = TextButton(
      child: const Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget deleteButton = TextButton(
      child: const Text(
        "Delete",
        style: TextStyle(color: Colors.red),
      ),
      onPressed: () {
        Navigator.of(context).pop();
        acceptDelete();
      },
    );
    Widget overwriteButton = TextButton(
      child: const Text(
        "Overwrite",
        style: TextStyle(color: Colors.green),
      ),
      onPressed: () {
        Navigator.of(context).pop();
        acceptOverwrite();
      },
    );

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text("Custom Drinks"),
        actions: const [
          Padding(
            padding: EdgeInsets.all(15),
            child: Icon(Icons.sports_bar_outlined),
          ),
        ],
        centerTitle: true,
        backgroundColor: const Color.fromARGB(255, 239, 244, 249),
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 10),
            // Bottle-Selection
            CarouselSlider.builder(
              itemCount: widget.coolingData.bottlesList.length,
              carouselController: _controller,
              options: CarouselOptions(
                aspectRatio: 2 / 1,
                viewportFraction: 1 / 2.7,
                enableInfiniteScroll: false,
                enlargeCenterPage: true,
                enlargeFactor: 0.45,
                initialPage: _currentPageIndex,
                onPageChanged: (index, reason) {
                  setState(() {
                    _currentPageIndex = index;
                  });
                },
              ),
              itemBuilder: (context, index, realIndex) {
                bool isActive = (widget.coolingData.bottlesList.elementAt(index) == _bottleToChange);
                return InkResponse(
                  radius: 25,
                  child: (realIndex < widget.coolingData.bottlesList.length - 1)
                      ? CarouselBottleElement(
                          bottle: widget.coolingData.bottlesList.elementAt(index),
                          isActive: isActive,
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.add,
                              color: isActive ? Colors.lightBlueAccent : null,
                              size: 50,
                            ),
                            Flexible(
                              child: Text(
                                "Add your own\nbottle here",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                  onTap: () {
                    _controller.animateToPage(
                      realIndex,
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.linearToEaseOut,
                    );
                    setState(() {
                      _bottleToChange = widget.coolingData.bottlesList.elementAt(index);
                      _tempBottle = _bottleToChange.copyWith();
                      _tempBottle = _bottleToChange.copyWith(
                          isDeleteProtected: (index == widget.coolingData.bottlesList.length - 1) ? false : null);
                      _displayNameController.text = _tempBottle.displayName;
                      _startingTemperature = 25;
                      _endingTemperature = _tempBottle.desiredTemperature;
                      _freezerTemperature = -18;
                      _elapsedMinutes =
                          _tempBottle.approximateDuration(_freezerTemperature, _startingTemperature).inMinutes.round();
                    });
                  },
                );
              },
            ),
            Center(
              child: AnimatedSmoothIndicator(
                activeIndex: _currentPageIndex,
                count: widget.coolingData.bottlesList.length,
                effect: ExpandingDotsEffect(
                  activeDotColor: Colors.lightBlueAccent,
                  dotColor: Colors.grey.shade400,
                  dotWidth: 5,
                  dotHeight: 5,
                ),
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            // Display Name
            SettingsPageElement(
              title: "Display Name",
              tooltiptext: "Will be displayed when choosing\nthis type of drink in the configurator.",
              child: Padding(
                padding: const EdgeInsets.fromLTRB(70, 15, 70, 20),
                child: TextField(
                  controller: _displayNameController,
                  textAlign: TextAlign.center,
                  maxLength: 16,
                  textCapitalization: TextCapitalization.words,
                  keyboardType: TextInputType.text,
                  decoration: const InputDecoration(
                    hintText: "Puntigamer 0.5L",
                    helperText: "Keep it short",
                    isDense: true,
                    isCollapsed: false,
                    border: UnderlineInputBorder(),
                  ),
                  onChanged: (value) => _tempBottle.displayName = value,
                ),
              ),
            ),
            // Desired-Temperatur-Setter
            SettingsPageElement(
              title: "Type and Desired Temperature",
              tooltiptext:
                  "Set a bottletype by tapping the icon.\nWhen the desired temperature is reached,\nthe timer will ring.",
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 15.0),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: InkResponse(
                        radius: 20,
                        child: Icon(_tempBottle.icon, size: 50),
                        onTap: () {
                          setState(() {
                            _tempBottle.material = (_tempBottle.material == BottleMaterial.glass)
                                ? BottleMaterial.aluminum
                                : BottleMaterial.glass;
                          });
                        },
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: CustomDescribedSlider(
                        title: Text(
                          "Desired Temperature",
                          style: GoogleFonts.roboto(fontWeight: FontWeight.w300, fontSize: 13),
                          textAlign: TextAlign.center,
                        ),
                        valueSuffix: "°C",
                        value: _tempBottle.desiredTemperature,
                        min: 6,
                        max: 15,
                        divisions: 15 - 6,
                        flexRatio: 1 / 3,
                        onChanged: (value) {
                          setState(() {
                            _tempBottle.desiredTemperature = value;
                            _startingTemperature = 25;
                            _endingTemperature = _tempBottle.desiredTemperature;
                            _freezerTemperature = -18;
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            //Coefficient-Calculator
            SettingsPageElement(
              title: "Calculate Cooling Coefficient",
              tooltiptext:
                  "Measure the drink's starting temperature\nand put it in the freezer.\n\nTake it out after the elapsed time\nand note the ending temperature.\n\nThis way, the cooling coefficient of the\nfreezer can be calibrated.",
              child: Padding(
                padding: const EdgeInsets.fromLTRB(10, 15, 15, 15),
                child: Column(
                  children: [
                    CustomDescribedSlider(
                      title: Text(
                        "Freezer Temperature",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w300, fontSize: 13),
                        textAlign: TextAlign.center,
                      ),
                      valueSuffix: "°C",
                      value: _freezerTemperature,
                      min: -25,
                      max: _tempBottle.desiredTemperature - 1,
                      divisions: ((_tempBottle.desiredTemperature.toInt() - 1) - (-25)),
                      onChanged: (value) {
                        setState(() {
                          _endingTemperature = _tempBottle.desiredTemperature;
                          _freezerTemperature = value;
                        });
                      },
                    ),
                    const SizedBox(height: 20),
                    CustomDescribedSlider(
                      title: Text(
                        "Starting Temperature",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w300, fontSize: 13),
                        textAlign: TextAlign.center,
                      ),
                      valueSuffix: "°C",
                      value: _startingTemperature,
                      min: _tempBottle.desiredTemperature + 2,
                      max: 35,
                      divisions: 35 - (_tempBottle.desiredTemperature.toInt() + 2),
                      onChanged: (value) {
                        setState(() {
                          _startingTemperature = value;
                          _endingTemperature = (_endingTemperature > _startingTemperature.toInt() - 1)
                              ? _startingTemperature.toInt() - 1
                              : _endingTemperature;
                        });
                      },
                    ),
                    const SizedBox(height: 20),
                    CustomDescribedSlider(
                      title: Text(
                        "Ending Temperature",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w300, fontSize: 13),
                        textAlign: TextAlign.center,
                      ),
                      valueSuffix: "°C",
                      roundToPrecision: 1,
                      value: _endingTemperature,
                      min: _freezerTemperature + 1,
                      max: _startingTemperature - 1,
                      divisions: 2 * ((_startingTemperature.toInt() - 1) - (_freezerTemperature.toInt() + 1)),
                      onChanged: (value) {
                        setState(() {
                          _endingTemperature = value;
                        });
                      },
                    ),
                    const SizedBox(height: 20),
                    CustomDescribedSlider(
                      title: Text(
                        "Elapsed Minutes",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.w300, fontSize: 13),
                        textAlign: TextAlign.center,
                      ),
                      valueSuffix: "min",
                      value: _elapsedMinutes.toDouble(),
                      min: 3,
                      max: 120,
                      onChanged: (value) {
                        setState(() {
                          _elapsedMinutes = value.round();
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
            // Buttons
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 30, 30, 60),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // Delete Entry
                  FilledButton.tonalIcon(
                    icon: const Icon(Icons.delete_forever, color: Colors.red),
                    label: const Text("Delete"),
                    statesController: MaterialStatesController(),
                    onPressed: (_bottleToChange.isDeleteProtected == true)
                        ? null
                        : () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: const Text("Hold on.."),
                                  content:
                                      Text("Do you really want to delete the entry '${_bottleToChange.displayName}'?"),
                                  actions: [
                                    cancelButton,
                                    deleteButton,
                                  ],
                                );
                              },
                            );
                          },
                  ),
                  // Add/Overwrite entry
                  FilledButton.tonalIcon(
                      icon: const Icon(
                        Icons.done,
                        color: Colors.green,
                      ),
                      label: const Text("Accept"),
                      onPressed: (_bottleToChange == widget.coolingData.bottlesList.last)
                          ? () => acceptOverwrite()
                          : () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                        title: const Text("Overwriting..."),
                                        content: Text(
                                            "Do you really want to overwrite the entry '${_bottleToChange.displayName}'?"),
                                        actions: [
                                          cancelButton,
                                          overwriteButton,
                                        ]);
                                  });
                            }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
