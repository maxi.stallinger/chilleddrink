import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class SettingsPageElement extends StatelessWidget {
  final String title;
  final String tooltiptext;
  final Widget child;
  const SettingsPageElement({super.key, required this.title, required this.tooltiptext, required this.child});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              title,
              style: GoogleFonts.roboto(fontWeight: FontWeight.w400, fontSize: 14),
            ),
            Tooltip(
              message: tooltiptext,
              triggerMode: TooltipTriggerMode.tap,
              showDuration: const Duration(seconds: 6),
              textStyle: GoogleFonts.roboto(fontWeight: FontWeight.w300, fontSize: 12, color: Colors.white),
              child: const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Icon(Icons.info_outline, size: 18),
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
          child: Card(
            color: const Color.fromARGB(255, 239, 244, 249),
            surfaceTintColor: Colors.transparent,
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30),
            ),
            child: child,
          ),
        ),
      ],
    );
  }
}
