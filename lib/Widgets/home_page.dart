import 'package:chilleddrink/Dataclasses/custom_icons_icons.dart';
import 'package:chilleddrink/Dataclasses/shared_prefs.dart';
import 'package:chilleddrink/Widgets/carousel/carousel.dart';
import 'package:chilleddrink/Widgets/configurator/configurator.dart';
import 'package:chilleddrink/Widgets/countdown/countdown_controls.dart';
import 'package:chilleddrink/Widgets/settings/settings.dart';
import 'package:feature_discovery/feature_discovery.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/material.dart';
import 'package:chilleddrink/Dataclasses/cooling_data.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  late CoolingData coolingData;

  @override
  void initState() {
    coolingData = CoolingData.initial(vsync: this);
    coolingData.configurationChangedNotifier.addListener(configurationChanged);
    SharedPrefs.readLastUsedBottle(coolingData);
    coolingData.notifyListeners();
    SchedulerBinding.instance.addPostFrameCallback((Duration duration) {
      FeatureDiscovery.discoverFeatures(context, const <String>{"feature_cooling_configurator", "feature_settings"});
    });
    if (kDebugMode) {
      FeatureDiscovery.clearPreferences(context, <String>{"feature_cooling_configurator", "feature_settings"});
    }
    super.initState();
  }

  void configurationChanged() {
    setState(() {
      coolingData.updateCurrentTemperature();
      coolingData.updateDuration();
      coolingData.updateChartData();
    });
  }

  @override
  void dispose() {
    coolingData.configurationChangedNotifier.removeListener(configurationChanged);
    coolingData.countdown.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          // Cooling Configuration
          Expanded(
            flex: 4,
            child: DescribedFeatureOverlay(
              featureId: "feature_cooling_configurator",
              tapTarget: const Icon(CustomIcons.beer_bottle, color: Colors.white, size: 50),
              title: const Text("Configuration"),
              description: const Text(
                "Adjust settings like:\n  - Desired Temperature\n  - Starting Temperature\n  - Freezer Temperature",
                textAlign: TextAlign.left,
              ),
              targetColor: Colors.lightBlue.shade200,
              backgroundColor: const Color.fromARGB(255, 220, 234, 249),
              backgroundOpacity: 0.93,
              textColor: Colors.black,
              child: CoolingConfigurator(coolingData: coolingData),
            ),
          ),
          // Graphs
          Flexible(
            flex: 12,
            child: Carousel(coolingData: coolingData),
          ),
          // Countdown-Controls
          Expanded(
            flex: 3,
            child: Stack(
              alignment: Alignment.center,
              fit: StackFit.expand,
              children: [
                CountdownControls(coolingData: coolingData),
                Positioned(
                  bottom: 10,
                  left: 10,
                  child: DescribedFeatureOverlay(
                    featureId: "feature_settings",
                    tapTarget: const Icon(Icons.settings, color: Colors.white, size: 32),
                    title: const Text("Customization"),
                    description: const Text("Add your own bottles with custom durations"),
                    targetColor: Colors.lightBlue.shade200,
                    backgroundColor: const Color.fromARGB(255, 220, 234, 249),
                    backgroundOpacity: 0.93,
                    textColor: Colors.black,
                    child: SettingsButton(coolingData: coolingData),
                  ),
                ),
                Positioned(
                  bottom: 10,
                  left: 10,
                  child: DescribedFeatureOverlay(
                    featureId: "feature_settings",
                    tapTarget: const Icon(Icons.settings, color: Colors.white, size: 32),
                    title: const Text("Customization"),
                    description: const Text("Add your own bottles with custom durations"),
                    targetColor: Colors.lightBlue.shade200,
                    backgroundColor: const Color.fromARGB(255, 220, 234, 249),
                    backgroundOpacity: 0.93,
                    textColor: Colors.black,
                    child: SettingsButton(coolingData: coolingData),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
