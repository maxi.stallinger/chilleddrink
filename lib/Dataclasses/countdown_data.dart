import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CountdownData extends AnimationController {
  late double remainingProgress;
  // Constructor
  CountdownData({required super.vsync, super.duration, this.remainingProgress = 1.0}) {
    value = 1.0;
  }

  // Getter
  Duration get pendingTime => duration! * remainingProgress;
  Duration get elapsedTime => duration! * (1 - remainingProgress);
  DateTime get estimatedTimeOfArrival => DateTime.now().add(pendingTime);
  String get text {
    NumberFormat f = NumberFormat("00");

    final hours = pendingTime.inHours;
    final minutes = pendingTime.inMinutes.remainder(60);
    final seconds = pendingTime.inSeconds.remainder(60);

    if (hours >= 1) {
      return "${f.format(hours)} : ${f.format(minutes)} : ${f.format(seconds)}";
    } else if (minutes >= 1) {
      return "$minutes : ${f.format(seconds)}";
    } else if (seconds >= 1) {
      return "$seconds";
    } else {
      return "Chilled";
    }
  }
}
