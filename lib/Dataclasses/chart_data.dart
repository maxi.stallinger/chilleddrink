class ChartData {
  late DateTime time;
  late num temperature;

  ChartData(this.time, this.temperature);

  @override
  String toString() {
    return "$time: $temperature °C";
  }
}
