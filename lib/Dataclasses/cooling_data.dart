import 'dart:math';
import 'package:chilleddrink/Dataclasses/chart_data.dart';
import 'package:chilleddrink/Dataclasses/bottle.dart';
import 'package:chilleddrink/Dataclasses/countdown_data.dart';
import 'package:chilleddrink/Dataclasses/shared_prefs.dart';
import 'package:flutter/material.dart';

class CoolingData {
  late Bottle activeBottle;
  late List<Bottle> bottlesList;
  late double startTemperature;
  late double endTemperature;
  late double freezerTemperature;
  late double _currentTemperature;
  late List<ChartData> _chartData;
  late CountdownData _countdown;
  late ValueNotifier<bool> _configurationChangedNotifier;

  // Named Constructor
  CoolingData.initial({required TickerProvider vsync}) {
    SharedPrefs.readBottlesList(this);
    activeBottle = bottlesList.first;
    startTemperature = 25;
    endTemperature = activeBottle.desiredTemperature;
    freezerTemperature = -18;
    _configurationChangedNotifier = ValueNotifier(false);
    _countdown = CountdownData(vsync: vsync);
    updateDuration();
    updateCurrentTemperature();
    updateChartData();
  }

  // Getter
  double get currentTemperature => _currentTemperature;
  CountdownData get countdown => _countdown;
  List<ChartData> get chartData => _chartData;
  ValueNotifier<bool> get configurationChangedNotifier => _configurationChangedNotifier;

  // Setter
  set duration(Duration duration) {
    _countdown.duration = duration;
  }

  // Methods
  double calculateTemperature(Duration durationFromInitial) {
    return freezerTemperature +
        (startTemperature - freezerTemperature) * exp(-activeBottle.coeff * durationFromInitial.inSeconds);
  }

  void updateDuration() {
    _countdown.duration = Duration(
        seconds: -log((endTemperature - freezerTemperature) / (startTemperature - freezerTemperature)) ~/
            activeBottle.coeff);
  }

  void updateCurrentTemperature() {
    _currentTemperature = calculateTemperature(_countdown.elapsedTime);
  }

  void updateChartData() {
    int steps = 6;
    double res = _countdown.duration!.inSeconds / steps;
    var list = <ChartData>[];

    for (var i = 0; i <= steps; i++) {
      Duration timeOffset = Duration(seconds: (i * res).round());
      DateTime time = DateTime.now().add(timeOffset);
      num temperature = num.parse(calculateTemperature(timeOffset).toStringAsFixed(1));

      list.add(ChartData(time, temperature));
    }
    _chartData = list;
  }

  void notifyListeners() {
    _configurationChangedNotifier.value = !_configurationChangedNotifier.value;
  }
}
