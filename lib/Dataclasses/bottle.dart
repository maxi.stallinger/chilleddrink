import 'dart:convert';
import 'dart:math';

import 'package:chilleddrink/Dataclasses/custom_icons_icons.dart';
import 'package:flutter/material.dart';

enum BottleMaterial {
  glass,
  aluminum;

  String toJson() => name;
  static BottleMaterial fromJson(String json) => values.byName(json);
}

class Bottle {
  late BottleMaterial material;
  late double coeff;
  late double desiredTemperature;
  late String displayName;
  late bool _isDeleteProtected;

  // Constructor
  Bottle(this.material, this.coeff, this.desiredTemperature, this.displayName, this._isDeleteProtected);

  // Getters
  bool get isDeleteProtected => _isDeleteProtected;
  IconData get icon {
    return (material == BottleMaterial.glass) ? CustomIcons.beer_bottle : CustomIcons.beer_can;
  }

  // Default Beverages (Glass Bottle)
  static List<Bottle> defaultBottles() {
    return [
      Bottle(BottleMaterial.glass, 0.000166205344457245000, 8, "Glass 0.5L", true),
      Bottle(BottleMaterial.aluminum, 0.000230358780985385000, 8, "Aluminum 0.5L", true),
      Bottle(BottleMaterial.glass, 0.000166205344457245000, 8, "Custom Bottle 1", true),
    ];
  }

  // Update Coefficient of this bottle
  void updateCoefficient(double startTemperature, double endTemperature, double freezerTemperature, int seconds) {
    coeff = -log((endTemperature - freezerTemperature) / (startTemperature - freezerTemperature)) / seconds;
  }

  // Pre-calculate duration with given inputs (approximate, for Add/edit page)
  Duration approximateDuration(double freezerTemperature, double initialTemperature) {
    Duration duration = Duration(
        seconds: -log((desiredTemperature - freezerTemperature) / (initialTemperature - freezerTemperature)) ~/ coeff);
    if (duration.inMinutes.round() < 3) {
      return const Duration(minutes: 3);
    } else if (duration.inMinutes.round() > 120) {
      return const Duration(minutes: 120);
    } else {
      return duration;
    }
  }

  // Deep-Copy with optionally changable parameters
  Bottle copyWith({
    BottleMaterial? material,
    double? coeff,
    double? desiredTemperature,
    String? displayName,
    bool? isDeleteProtected,
  }) {
    return Bottle(
      material ?? this.material,
      coeff ?? this.coeff,
      desiredTemperature ?? this.desiredTemperature,
      displayName ?? this.displayName,
      isDeleteProtected ?? _isDeleteProtected,
    );
  }

  // Serialization
  factory Bottle.fromJson(Map<String, dynamic> jsonData) {
    return Bottle(BottleMaterial.fromJson(jsonData["type"]), jsonData["coeff"], jsonData["desiredTemperature"],
        jsonData["displayName"], jsonData["isDeleteProtected"]);
  }

  static Map<String, dynamic> toMap(Bottle bottle) => {
        "type": bottle.material.toJson(),
        "coeff": bottle.coeff,
        "desiredTemperature": bottle.desiredTemperature,
        "displayName": bottle.displayName,
        "isDeleteProtected": bottle._isDeleteProtected
      };

  static String encode(List<Bottle> bottles) =>
      jsonEncode(bottles.map<Map<String, dynamic>>((bottle) => Bottle.toMap(bottle)).toList());

  static List<Bottle> decode(String bottles) =>
      (jsonDecode(bottles) as List<dynamic>).map<Bottle>((item) => Bottle.fromJson(item)).toList();
}
