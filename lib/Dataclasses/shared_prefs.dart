import 'package:chilleddrink/Dataclasses/bottle.dart';
import 'package:chilleddrink/Dataclasses/cooling_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  // Initialization
  static late final SharedPreferences instance;
  static Future<SharedPreferences> init() async => instance = await SharedPreferences.getInstance();

  // Keys
  static const String _keyBottlesList = "bottles_key";
  static const String _keyLastUsedBottle = "last_used_bottle";
  static const String _keyScheduledTime = "scheduled_timestamp";
  static const String _keyOldBottlesListIsDeleted = "oldBottlesListIsDeleted";

  // Methods
  static void checkAndDeleteOldBottlesList() async {
    if (instance.containsKey(_keyOldBottlesListIsDeleted) == false) {
      instance.remove(_keyBottlesList);
      instance.remove(_keyLastUsedBottle);
      await instance.setInt(_keyOldBottlesListIsDeleted, 1);
    }
  }

  static void readBottlesList(CoolingData coolingData) {
    checkAndDeleteOldBottlesList();
    if (instance.containsKey(_keyBottlesList)) {
      coolingData.bottlesList = Bottle.decode(instance.getString(_keyBottlesList)!);
    } else {
      coolingData.bottlesList = Bottle.defaultBottles();
      writeBottlesList(coolingData);
    }
  }

  static void writeBottlesList(CoolingData coolingData) {
    instance.setString(_keyBottlesList, Bottle.encode(coolingData.bottlesList));
  }

  static void readLastUsedBottle(CoolingData coolingData) {
    if (instance.containsKey(_keyLastUsedBottle)) {
      int index = instance.getInt(_keyLastUsedBottle)!;
      coolingData.activeBottle = (index < coolingData.bottlesList.length - 1)
          ? coolingData.bottlesList.elementAt(index)
          : coolingData.activeBottle = coolingData.bottlesList.first;
    } else {
      coolingData.activeBottle = coolingData.bottlesList.first;
    }
  }

  static Future<void> writeLastUsedBottle(CoolingData coolingData) async {
    await instance.setInt(_keyLastUsedBottle, coolingData.bottlesList.indexOf(coolingData.activeBottle));
  }

  static Future<void> writeScheduledTime(CoolingData coolingData) async {
    await instance.setString(_keyScheduledTime, coolingData.chartData.last.time.toIso8601String());
  }

  static void readScheduledTime(CoolingData coolingData) {
    if (instance.containsKey(_keyScheduledTime)) {
      DateTime storedTimeFinished = DateTime.parse(instance.getString(_keyScheduledTime)!);
      Duration remainingTime = storedTimeFinished.difference(DateTime.now());
      double correctedProgress = remainingTime.inMicroseconds / coolingData.countdown.duration!.inMicroseconds;
      coolingData.countdown.remainingProgress = (correctedProgress < 0.0) ? 0 : correctedProgress;
    }
  }

  static Future<void> removeScheduledTime() async {
    if (instance.containsKey(_keyScheduledTime)) {
      await instance.remove(_keyScheduledTime);
    }
  }
}


/*
Sentry.captureMessage(
          "Correcting from '${countdown.remainingProgress}' to '$correctedProgress' -- Corrected remaining time: '${difference.toString()}' -- ScheduledTime: '${storedTimeFinished.toIso8601String()}' -- Calculated ETA: '${DateTime.now().add(Duration(seconds: (correctedProgress * countdown.duration!.inSeconds).toInt()))}'");

Sentry.captureMessage(
          "Corrected: remainingProgress: '${countdown.remainingProgress}', value: '${countdown.value}', isAnimating: '${countdown.isAnimating}'");
*/